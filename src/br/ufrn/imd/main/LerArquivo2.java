package br.ufrn.imd.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class LerArquivo2 {
  public static void main(String[] args) throws IOException {
    InputStream is = new FileInputStream("saida.txt");
    Scanner entrada = new Scanner(is);

    while (entrada.hasNextLine()) {
      System.out.println(entrada.nextLine());
    }
    entrada.close();
  }
}